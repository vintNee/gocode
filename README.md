## 实用项目表
### 1.popular
- [hutool](https://gitee.com/loolly/hutool)
- [jfinal](https://gitee.com/jfinal/jfinal)
- [heartbeat](https://gitee.com/mkk/HeartBeat)
- [ip2region](https://gitee.com/lionsoul/ip2region)
### 2.group
- [xxl](http://www.xuxueli.com/page/projects.html)
### 3.list
- [xxl-job分布式任务调度平台](https://gitee.com/xuxueli0323/xxl-job)
- [jfinal-weixin](https://gitee.com/jfinal/jfinal-weixin)
- [bugs-fly一个jfinal实现的bug管理平台
](https://gitee.com/tai/bugs-fly)
### 4.cms
- [jrelax](https://gitee.com/zengchao/jrelax)
- [platform](https://gitee.com/oschina12/platform)
### 5.oa
- [lemon一个javaoa](https://gitee.com/xuhuisheng/lemon)

### 6.语言
#### python
- [maze](https://gitee.com/jollyson/maze)
#### java
- [image-plugin图片处理插件](https://gitee.com/biezhi/image-plugin)
#### c/c++
- [c文件配置](https://gitee.com/brisk/conf-c)
- [tbox c库](https://gitee.com/tboox/tbox)

### 7.interview
- [ms100](https://gitee.com/zfoolt/ms100)
- [CrackingTheCodingInterview](https://gitee.com/oymyisme/CrackingtheCodingInterview-Practice)


### 8.tools
- [forest一个简单的Http库](https://gitee.com/dt_flys/forest)
- [pinyin汉字转为拼音](https://gitee.com/duguying2008/pinyin)
- [remote-tail远程tail工具](https://gitee.com/orionis/remote-tail)
- [asciilize把图片转化为ascii的工具](https://gitee.com/wangfuying/asciilize)
- [MD2file，md文件的导出工具](https://gitee.com/cevin15/MD2File)
- [xmonitor-client一款linux的监控工具](https://gitee.com/snail/xmonitor-client)
- [mdeditor md的编辑器](https://gitee.com/qinshenxue/mdeditor)
- [Direetory-Helper 目录同步助手](https://gitee.com/J_Sky/Direetory-Helper)
### 9.小游戏
- [ShootPlane打飞机](https://gitee.com/jemygraw/ShootPlane)
- [Plane-war 一个java版的打飞机游戏](https://gitee.com/003/Plane-War)
### 10.video/audio
- [ffplay一个java的音乐播放器](https://gitee.com/jflyfox/FFPlayer)
- [avplay](https://gitee.com/jackarain/avplayer)
- [OPMP3Play](https://gitee.com/edwinfound/OPMP3Player)
### 其它
- [urls短链接生成服务](https://gitee.com/hao_jiayu/urls)